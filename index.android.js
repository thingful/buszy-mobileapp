import React from 'react';
import { AppRegistry } from 'react-native';
import App from './app';

AppRegistry.registerComponent('buszy_react', () => App);

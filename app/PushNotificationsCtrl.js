import React, { Component } from 'react';
import PushNotification from 'react-native-push-notification';
import {
  AppState
} from 'react-native';


export default class PushNotificationsCtrl extends Component {

  constructor(props) {
    super(props);
  }

  a() {
    PushNotification.localNotificationSchedule({
      message: "My Notification Message",
      date: new Date(Date.now() + (60 * 1000))
    });
  }

  componentWillMount() {
    AppState.addEventListener('change', this.a);
  }

  componenetDidMount() {
    PushNotification.configure({
      onNotification: function(notification) {
        console.log( 'NOTIFICATION:', notification );
      },

      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
          alert: true,
          badge: true,
          sound: true
      },

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
        * (optional) default: true
        * - Specified if permissions (ios) and token (android and ios) will requested or not,
        * - if not, you must call PushNotificationsHandler.requestPermissions() later
        */
      requestPermissions: true,
    });
  }

  render() {
    return null;
  }
}
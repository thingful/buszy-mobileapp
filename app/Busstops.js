import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  Picker,
  AppState
} from 'react-native';

import PushNotificationsCtrl from './PushNotificationsCtrl';
import PushNotification from 'react-native-push-notification';


const stylesheet = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {},
  buttonText: {},
  picker:{
    width: 100,
  }
});

class BusStops extends Component{
  constructor(props) {
    super(props);
    this.state = {
      seconds: 5,
    }

    this.handleAppStateChange = this.handleAppStateChange.bind(this);
  }

  static get defaultProps() {
    return {
      title: 'Bus Stops'
    };
  }

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  handleAppStateChange(appState) {
     // if (appState === 'background') {
        console.log('app in the backgorund');

        PushNotification.localNotificationSchedule({
          message: "My Notification Message",
          date: new Date(Date.now() + (60 * 1000))
        });
     // }
  }

  _onButtonPressed() {
    this.props.navigator.pop();
  }

  render() {
    return (
      <View style={stylesheet.container}>
        <Text>{this.props.title}</Text>
        <TouchableHighlight onPress={this._onButtonPressed.bind(this)} style={stylesheet.button}>
          <Text style={stylesheet.buttonText}>{this.props.data.commonName}</Text>
        </TouchableHighlight>

        <Text>Send notification in seconds</Text>

        <Picker
          style={stylesheet.picker}
          selectedValue={this.state.seconds}
          onValueChange={(seconds) => this.setState({ seconds })}
        >
          <Picker.Item label="5" value={5} />
          <Picker.Item label="10" value={10} />
          <Picker.Item label="15" value={15} />
        </Picker>

        <PushNotificationsCtrl />
      </View>
    );
  }
}

module.exports = BusStops;


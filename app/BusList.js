import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableHighlight,
} from 'react-native';


class BusList extends Component{
  constructor(props) {
    super(props);
    this.state = {
      busData: []
    };
  }

  componentDidMount() {
     _getNearestBusStops(this);
  }

  _onButtonPressed(data) {
    this.props.navigator.push({
      id : 'Busstops',
      data : data
    });
  }

  render() {
    var _scrollView = ScrollView;

    return (
      <View style={stylesheet.window}>
        <ScrollView
          ref={(scrollView) => { _scrollView = scrollView; }}
          automaticallyAdjustContentInsets={false}
          onScroll={() => { console.log('onScroll!'); }}
          scrollEventThrottle={200}
          style={stylesheet.scrollView}
        >

          <View style={stylesheet.container}>
          { this.state.busData.map( (data, index) => {
              return <TouchableHighlight key={index} onPress={this._onButtonPressed.bind(this, data)} style={stylesheet.button}>
                       <Text style={stylesheet.buttonText}>{data.commonName}</Text>
                     </TouchableHighlight>
            })
          }
          </View>

        </ScrollView>
      </View>
    );
  }
}

function _getNearestBusStops(obj) {
  return fetch('https://buszy-server.herokuapp.com/bus-stops?lat=51.52335&lon=-0.08394')
    .then((response) => response.json())
    .then((json) => {
      obj.setState({ busData : json.response });
    })
    .catch((error) => {
      console.error(error);
    });
}

const stylesheet = StyleSheet.create({
  window: {
    marginTop: 0,
    height: 700
  },
  nav: {
    marginTop: 20,
  },
  scrollView: {
    marginTop: 60,
    backgroundColor: '#FFF',
    height: 400
  },
  container: {
    height: window.height,
    marginBottom: 40,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  button: {
    backgroundColor: '#8BDCFF',
    width: 300,
    height: 50,
    margin: 10,
    paddingTop:15,
    borderRadius:10
  },
  buttonText: {
    backgroundColor: 'transparent',
    color: '#0A0A0A',
    textAlign: 'center'
  }
});

module.exports = BusList;

import React, {Component} from 'react';
import {
  Navigator,
  View,
  StyleSheet
} from 'react-native';
import PushNotificationsCtrl from './PushNotificationsCtrl';

// import views
var BusList  = require('./BusList');
var Busstops = require('./Busstops');


// style
const styles = StyleSheet.create({
  nav: {
    marginTop: 20,
  }
});

// Navigator component
export default class App extends Component {
  constructor(props) {
    super(props);
  }

  _navigatorRenderScene(route, navigator) {
    var _navigator = navigator;

    switch (route.id) {
      case 'BusList':
        return (<BusList navigator={navigator} />);
        break;
      case 'Busstops':
        return (<Busstops navigator={navigator} data={route.data} />);
        break;
    }
  }

  render() {
    return (
      <Navigator
        style={styles.nav}
        initialRoute={{ id: 'BusList', index: 0 }}
        renderScene={
          this._navigatorRenderScene
        }
      />
    );
  }
}